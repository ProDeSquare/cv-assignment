-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 10, 2021 at 10:26 AM
-- Server version: 10.5.9-MariaDB
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cv`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_home` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `about` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualifications` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `experience` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `address`, `phone_home`, `phone`, `image`, `about`, `qualifications`, `experience`) VALUES
(1, 'Hamza Mughal', 'hamza@prodesquare.com', '123 Main Street, San Francisco, 94122', '+92 335 972 0072', '+92 303 631 0300', 'uploads/QTYP3698.JPG', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mi ex, commodo in augue a, imperdiet commodo ante. Etiam interdum metus ultrices, egestas erat nec, sollicitudin turpis. Integer ac lectus ultrices, porttitor nisl non, gravida purus. Nulla a dui id purus laoreet vehicula. Phasellus quis felis vestibulum justo blandit feugiat eu ut dolor. In eu dignissim diam, sit amet mattis erat. Donec molestie tortor et congue laoreet. Quisque pretium dui at tellus laoreet, placerat lobortis metus viverra. Donec justo libero, luctus sit amet rhoncus eget, blandit vitae velit. Fusce at enim tincidunt, posuere nunc at, egestas purus. In dapibus ultricies rhoncus. Ut imperdiet sollicitudin interdum. Morbi rhoncus nibh mauris, vitae bibendum nulla dictum ac. Curabitur ut tellus tristique, suscipit magna non, pulvinar velit. Nullam lectus ex, venenatis vitae velit id, hendrerit dignissim diam.', 'ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present)', 'ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present),ABC Software House (2012-present)'),
(2, 'Muzammil Shafique', 'muzammil@prodesquare.com', '123 Main Street, San Francisco, 94122', '+92 300 000 0000', '+92 300 000 0000', 'uploads/0047.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mi ex, commodo in augue a, imperdiet commodo ante. Etiam interdum metus ultrices, egestas erat nec, sollicitudin turpis. Integer ac lectus ultrices, porttitor nisl non, gravida purus. Nulla a dui id purus laoreet vehicula. Phasellus quis felis vestibulum justo blandit feugiat eu ut dolor. In eu dignissim diam, sit amet mattis erat. Donec molestie tortor et congue laoreet. Quisque pretium dui at tellus laoreet, placerat lobortis metus viverra. Donec justo libero, luctus sit amet rhoncus eget, blandit vitae velit. Fusce at enim tincidunt, posuere nunc at, egestas purus. In dapibus ultricies rhoncus. Ut imperdiet sollicitudin interdum. Morbi rhoncus nibh mauris, vitae bibendum nulla dictum ac. Curabitur ut tellus tristique, suscipit magna non, pulvinar velit. Nullam lectus ex, venenatis vitae velit id, hendrerit dignissim diam. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc mi ex, commodo in augue a, imperdiet commodo ante. Etiam interdum metus ultrices, egestas erat nec, sollicitudin turpis. Integer ac lectus ultrices, porttitor nisl non, gravida purus. Nulla a dui id purus laoreet vehicula. Phasellus quis felis vestibulum justo blandit feugiat eu ut dolor. In eu dignissim diam, sit amet mattis erat. Donec molestie tortor et congue laoreet. Quisque pretium dui at tellus laoreet, placerat lobortis metus viverra. Donec justo libero, luctus sit amet rhoncus eget, blandit vitae velit. Fusce at enim tincidunt, posuere nunc at, egestas purus. In dapibus ultricies rhoncus. Ut imperdiet sollicitudin interdum. Morbi rhoncus nibh mauris, vitae bibendum nulla dictum ac. Curabitur ut tellus tristique, suscipit magna non, pulvinar velit. Nullam lectus ex, venenatis vitae velit id, hendrerit dignissim diam.', 'I designed facebook,I designed twitter,I designed facebook,I designed twitter,I designed facebook,I designed twitter', 'i worked at facebook,i worked at twitter,i worked at facebook,i worked at twitter,i worked at facebook,i worked at twitter,i worked at facebook,i worked at twitter');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
