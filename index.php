<?php
    require_once 'database.php';
    
    $result = mysqli_query(
        $conn,
        "SELECT * FROM users"
    );
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
    />
    <title>CV</title>
  </head>

  <body class="container my-4">
    <header>
      <h1>Previously created CVs</h1>
      <a href="form.html">Create a new CV</a>
    </header>

    <main class="mt-4">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col"></th>
          </tr>
        </thead>

        <tbody>
          <?php while ($cv = mysqli_fetch_assoc($result)): ?>
            <tr>
              <th scope="row"><?= $cv['id'] ?></th>
              <td><?= $cv['name'] ?></td>
              <td><?= $cv['email'] ?></td>
              <td><a href="view.php?id=<?= $cv['id'] ?>">Visit</a></td>
            </tr>
          <?php endwhile ?>
        </tbody>
      </table>
    </main>
  </body>
</html>
