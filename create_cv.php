<?php

// if user is sending request with method other than post, kill the page
if ($_SERVER["REQUEST_METHOD"] !== "POST") die("Method not supported");

// database connection file
require_once './database.php';

// inputs
$name = $_POST['name'];
$email = $_POST['email'];
$address = $_POST['address'];
$phone_home = $_POST['phone_home'];
$phone = $_POST['phone'];
$about = $_POST['about'];
$qualifications = $_POST['qualifications'];
$experience = $_POST['experience'];
$image_target = "uploads/" . basename($_FILES['image']['name']);

if (!move_uploaded_file($_FILES['image']['tmp_name'], $image_target)) die("There was an error uploading your image");

mysqli_query(
    $conn,
    "INSERT INTO users (name, email, address, phone_home, phone, image, about, qualifications, experience)
    VALUES ('$name', '$email', '$address', '$phone_home', '$phone', '$image_target', '$about', '$qualifications', '$experience')"
);

header("Location: index.php");