<?php
require_once 'database.php';

$user = $_GET['id'];

$cv = mysqli_fetch_assoc(mysqli_query(
    $conn,
    "SELECT * FROM users WHERE id='$user'"
));

if (!$cv) die('<span style="font-family: monospace;">ERROR 404</span>');
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link
      rel="stylesheet"
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
    />
    <title>CV | <?= $cv['name'] ?></title>
  </head>
  <body class="container">
    <header class="row my-4">
      <div class="col-md-3">
        <img
          class="img-fluid"
          src="<?= $cv['image'] ?>"
          alt="<?= $cv['name'] ?>"
        />
      </div>

      <div class="col-md-6 text-center">
        <h1><?= $cv['name'] ?></h1>
        <div class="text-muted">
          <div><?= $cv['address'] ?></div>
          <div>Home: <?= $cv['phone_home'] ?> | Cell: <?= $cv['phone'] ?></div>
          <div><?= $cv['email'] ?></div>
        </div>
      </div>
    </header>

    <hr />

    <main>
      <section class="mb-4">
        <h2 class="text-primary">Professional Summary</h2>

        <p class="text-muted"><?= $cv['about'] ?></p>
      </section>

      <hr />

      <section class="mb-4">
        <h2 class="text-primary">Qualifications</h2>

        <ul>
          <?php foreach (explode(',', $cv['qualifications']) as $qualification): ?>
            <li><span class="text-muted"><?= $qualification ?></span></li>
          <?php endforeach ?>
        </ul>
      </section>

      <hr />

      <section class="mb-4">
        <h2 class="text-primary">Experience</h2>

        <ul>
          <?php foreach (explode(',', $cv['experience']) as $experience): ?>
            <li><?= $experience ?></li>
          <?php endforeach ?>
        </ul>
      </section>
    </main>
  </body>
</html>
